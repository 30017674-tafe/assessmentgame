﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public TextFileManager fileManager;
    public Text scoreText;

    // Start is called before the first frame update
    void Awake()
    {
        LoadScores();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LoadScores() 
    {
        fileManager.ReadFileContents(fileManager.logName); // reads the contents of the file with the given name
        for (int i = 0; i < 3; i++) // only shows the first 3 scores
        {
            string[] s = fileManager.GetPairByKey("HighScore #" + i.ToString());
            if (s.Length > 0)
            {
                scoreText.text += '\n' + fileManager.GetPairByKey(s[1])[0].ToUpper() + " " + fileManager.GetPairByKey(s[1])[1]; // adds the user's initials and time to the score text window
            }
        }
    }

    public void ResetScores()
    {
        fileManager.ClearFile(fileManager.logName); // removes all text in the file with the given name
    }
    public void StartGame() // sets the timescale to 1, turns off the cursor and locks it to the center of the screen, then loads the gameplay scene
    {
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        SceneManager.LoadScene(1);
    }

    public void RestartGame()
    {
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        SceneManager.LoadScene(1);
    }
    public void ExitGame() // quits the game
    {
        Application.Quit();
    }

    public void HSScreen() // turns the cursor back on and loads the highscore window
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        SceneManager.LoadScene(2);
    }

    public void MainMenu() // resets the timescale, turns the cursor back on 
    {
        Time.timeScale = 1;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        SceneManager.LoadScene(0);
    }

    public void Linktree() // sends the user to a linktree with my links (TFW in bottom corner of main menu, high score menu and pause menu)
    {
        Application.OpenURL("https://linktr.ee/tfwarcry");
    }
}
