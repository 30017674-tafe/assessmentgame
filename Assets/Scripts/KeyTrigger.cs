﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyTrigger : MonoBehaviour
{
    public KeyColour colour;


    private void OnTriggerEnter(Collider other) // adds the coloured key to the inventory when walked through
    {
        if (other.tag == "Player")
        {
            KeyInv keyInv = other.GetComponent<KeyInv>();
            if (keyInv.PickupKey(colour) == true)
            {
                gameObject.SetActive(false);
            }
        }
    }
}

public enum KeyColour { purple, blue, red, yellow, green }