﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float speed = 11.0f;
    public float sprintSpeed = 18.0f;
    public float gravity = 0.66f;
    public float jumpForce = 0.18f;
    public Vector3 originalPos;

    private float currentSpeed = 0;
    private float velocity = 0;
    private CharacterController controller;
    private Vector3 motion;

    // Awake is called before the Start function runs
    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        originalPos = transform.position;
        currentSpeed = speed;           // sets current speed to default when game is launched
        UIManager.instance.paused = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (UIManager.instance.paused == false)
        {
            if (Input.GetKeyDown(KeyCode.Escape) == true)
            {
                UIManager.instance.paused = true;
                UIManager.instance.PauseMenu();
            }
            motion = Vector3.zero;         // resets motion to 0
            if (controller.isGrounded == true) // checks for the object with a CharacterController to be on the ground
            {
                velocity = -gravity * Time.deltaTime;
                if (Input.GetButtonDown("Jump") == true) // sets upward velocity to the initial jump force on the first frame of the jump
                {
                    velocity = jumpForce;
                }
                else if (Input.GetButtonDown("Sprint") == true) // sets current speed to the sprint speed when sprint button is pressed
                {
                    if (currentSpeed != sprintSpeed)
                    {
                        currentSpeed = sprintSpeed;
                    }
                }
                else if (Input.GetButtonUp("Sprint") == true) // resets current speed to default when sprint button is released
                {
                    if (currentSpeed != speed)
                    {
                        currentSpeed = speed;
                    }
                }
            }
            else
            {
                velocity -= gravity * Time.deltaTime;
            }
            ApplyMovement();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape) == true) // lets the user exit the pause menu by pressing escape
            {
                UIManager.instance.paused = false;
                UIManager.instance.PauseMenu();
            }
        }
    }

    void ApplyMovement()
    {
        motion += transform.forward * Input.GetAxisRaw("Vertical") * currentSpeed * Time.deltaTime;
        motion += transform.right * Input.GetAxisRaw("Horizontal") * currentSpeed * Time.deltaTime;
        motion.y += velocity;
        controller.Move(motion);
    }

}
