﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyInv : MonoBehaviour
{
    private List<KeyColour> collectedKeys = new List<KeyColour>(); // list of collected keys that are of type keyColour (defined in KeyTrigger as an enum)

    public bool PickupKey(KeyColour colour)
    {
        if (collectedKeys.Contains(colour) == false) // if the key is not already in the collected keys list
        {
            collectedKeys.Add(colour); // adds the colour of the picked up key to the collected keys list
            Debug.Log("Player picked up the " + colour + " key!");
            if (UIManager.instance.countingTime == true)
            {
                GameMaster.instance.AddScore(50); // adds 50 score if the game has started - tutorial room key can't give score
            }
            return true;
        }
        return false;
    }

    public bool CheckKeys(KeyColour colour) // checks for the coloured key in the collected keys list
    {
        foreach (KeyColour c in collectedKeys)
        {
            if (c == colour)
            {
                return true;
            }
        }
        return false;
    }
}
