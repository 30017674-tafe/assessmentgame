﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingHazard : MonoBehaviour
{
    public float damage;

    private bool falling = false;
    private Transform tform; 

    void Start()
    {
        tform = GetComponent<Transform>();
    }

    void Update()
    {
        if (tform.rotation.z != 90.66f) // falling variable, lets the player stand on the object if it's not falling
        {
            falling = true;
        }
        if (GameMaster.instance.removeFloorObject == true) // removes falling object to stop it from getting stuck
        {
            gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            if (falling == true)
            {
                collision.collider.GetComponent<Health>().TakeDamage(damage);
                Debug.Log("Player took " + damage + " damage!");
            }
        }
    }
}
