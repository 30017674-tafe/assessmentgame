﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteraction : InteractableObject
{
    public bool locked = false;
    public KeyColour keyColour;

    private MeshRenderer rend;

    public override void Activate()
    {
        if (locked == false)
        {
            if (UIManager.instance.countingTime == true)
            {
                GameMaster.instance.AddScore(100); // stops the score from going up if the timer hasn't started
                                                   // tutorial door isn't worth any score
            }
            Debug.Log("Player just opened the " + keyColour + " door!");
            Animator targetAnim = GetComponent<Animator>();
            bool toggle = targetAnim.GetBool("Toggle Door");
            targetAnim.SetBool("Toggle Door", !toggle);
            if (keyColour == KeyColour.purple) // allows the falling object to be removed once the purple door has fallen - can get stuck
            {
                GameMaster.instance.purpleFallen = true;
            }
        }
        if (locked == true)
        {
            Debug.Log("You cannot open the " + keyColour + " door! You don't have the key yet!");
        }
    }

    public void UnlockDoor(bool activate)
    {
        if (locked == true)
        {
            locked = false;
            if (activate == true)
            {
                Activate();
            }
        }
    }
}
