﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimerTrigger : MonoBehaviour
{
    public TextFileManager fileManager;

    private float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        UIManager.instance.timerText.text = "CURRENT TIME:\n" + timer + "s"; // resets the timer text to the current timer value (0 by default) to prevent leak over from old attempts
        fileManager.CreateFile(fileManager.logName);  // creates a file with a given name
        fileManager.ReadFileContents(fileManager.logName); // reads the file with the given name
    }

    // Update is called once per frame
    void Update()
    {

        if (UIManager.instance.countingTime == true) // while the game is counting time, it increments the timer by the time and updates it in the ui manager.
        {
            timer += Time.deltaTime;
            UIManager.instance.UpdateTimer(timer);
        }

    }

    private void OnTriggerEnter(Collider other) // actual trigger script for the timer
    {
        if (other.tag == "Player")
        {
            UIManager.instance.countingTime = !UIManager.instance.countingTime; // inverts whether or not time is being counted
            if (UIManager.instance.countingTime == true) // prints a good luck message if the timer just got turned on
            {
                Debug.Log("Timer started! Good luck out there");
            }
            if (UIManager.instance.countingTime == false) // prints a well done message if the timer just got turned off
            {
                Debug.Log("Timer stopped. Well played!");
            }
            gameObject.SetActive(false); // turns off the timer trigger to prevent the player from starting and stopping instantly
        }
    }

    public void SaveTimeToFile() // saves the player's time to a file with their initials and the highscore number
    {
        for (int i = 0; i < 3; i++)
        {
            if (fileManager.LocateValueByKey("HighScore #" + i.ToString()) == "")
            {
                fileManager.AddKeyValuePair(fileManager.logName, "HighScore #" + i.ToString(), UIManager.instance.input.text);
                fileManager.AddKeyValuePair(fileManager.logName, UIManager.instance.input.text, timer.ToString());
                break;
            }
            else
            {
                string k = fileManager.LocateValueByKey("HighScore #" + i.ToString());
                if (k == UIManager.instance.input.text)
                {
                    float.TryParse(fileManager.LocateValueByKey(k), out float r);
                    if (r < timer)
                    {
                        fileManager.AddKeyValuePair(fileManager.logName, k, timer.ToString());
                        break;
                    }
                }
            }
        }
        UIManager.instance.input.gameObject.SetActive(false); // turns off the input field
        SceneManager.LoadScene(2); // sends the player to the high score window
    }
}
