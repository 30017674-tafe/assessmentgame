﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTrigger : MonoBehaviour
{
    public int score;

    // Update is called once per frame
    private void OnTriggerEnter(Collider other) // adds the score when the trigger is entered and removes the score trigger object
    {
        if (other.tag == "Player")
        {
            GameMaster.instance.AddScore(score);
            gameObject.SetActive(false);
        }
    }
}
