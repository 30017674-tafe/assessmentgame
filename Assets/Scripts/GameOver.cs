﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) // trigger for when to stop the game
    {
        if (other.tag == "Player")
        {
            GameMaster.instance.gameOver = true;
        }
        if (GameMaster.instance.gameOver == true)
        {
            Cursor.visible = true; // turns the cursor back on 
            Cursor.lockState = CursorLockMode.Confined; // makes it so the player can move the cursor around the window
            other.GetComponentInChildren<CameraControl>().enabled = false; // turns off the camera control
            other.GetComponent<PlayerControl>().enabled = false; // turns off the player's movement
            UIManager.instance.input.gameObject.SetActive(true); // turns the input text box on
            UIManager.instance.inputBG.gameObject.SetActive(true); // trusn the input background on
        }
    }
}
