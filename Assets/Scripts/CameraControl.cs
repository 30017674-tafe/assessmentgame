﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float sens = 2.5f;
    public float drag = 1.5f;

    private Transform character;
    private Vector2 direction;
    private Vector2 smoothing;
    private Vector2 result;

    private void Awake()
    {
        character = transform.root; // allows rotation of the player rather than just the camera
    }
    void Update()
    {
        if (UIManager.instance.paused == false) //  makes it so the player can't look around while the game is paused
        {
            direction = new Vector2(Input.GetAxisRaw("Mouse X") * sens, Input.GetAxisRaw("Mouse Y") * sens);
            smoothing = Vector2.Lerp(smoothing, direction, 1 / drag);
            result += smoothing;
            result.y = Mathf.Clamp(result.y, -60, 60); // limits how high/low the player can look

            transform.localRotation = Quaternion.AngleAxis(-result.y, Vector3.right);
            character.rotation = Quaternion.AngleAxis(result.x, character.up);
        }
    }
}
