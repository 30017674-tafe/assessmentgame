﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazards : MonoBehaviour
{
    public float damage;
    public bool healer = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            
            if (healer == true)
            {
                damage = -damage; // inverts damage to heal the player
                if (other.GetComponent<Health>().CurrentHealth != other.GetComponent<Health>().maxHealth) // if the player isn't at full health, it heals them and removes the health pack
                {
                    other.GetComponent<Health>().TakeDamage(damage);
                    gameObject.SetActive(false);
                }
                Debug.Log("Player healed for " + -damage + " health - currently at " + other.GetComponent<Health>().CurrentHealth); // outputs the current health to the console with a message to say they were healed
            }
            if (healer == false)
            {
                other.GetComponent<Health>().TakeDamage(damage);
                Debug.Log("Player took " + damage + " damage - currently at " + other.GetComponent<Health>().CurrentHealth); // outputs the current health to the console with a message to say they were damaged
                UIManager.instance.UpdateScore(-50);
            }
        }
    }
}