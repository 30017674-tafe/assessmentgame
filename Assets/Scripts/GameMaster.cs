﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public static GameMaster instance;

    public bool gameOver = false;
    public bool purpleFallen = false;
    public bool removeFloorObject = false;

    private float fallTime = 0f;
    private int currentScore = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        UIManager.instance.UpdateScore(currentScore);
    }

    private void Update()
    {
        if (purpleFallen == true) // starts counting from when the purple door is open in order to remove the falling object
        {
            fallTime += Time.deltaTime;
            if (fallTime >= 5f)
            {
                removeFloorObject = true;
            }
        }
    }
    public void AddScore(int score) // increments the user's score by the score attached to an object/trigger
    {
        currentScore += score;
        Debug.Log(currentScore);
        UIManager.instance.UpdateScore(currentScore);
    }

    public void ReturnToMenu() // sends the player to the main menu
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);
    }

    
}
