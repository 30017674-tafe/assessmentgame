﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Image healthBar;
    public Text healthText;
    public Text scoreText;
    public Text timerText;
    public InputField input;
    public bool countingTime = false;
    public Image crosshair;
    public Sprite interactableCrosshair;
    public Sprite baseCrosshair;
    public Image inputBG;
    public Image pauseMenu;
    public bool paused = false;


    private int score = 0;
    public static UIManager instance;
    private void Awake() // sets the score text to the default score (0) to stop leaking from other attempts. turns the input menu and pause menu off
    {
        if (instance == null)
        {
            instance = this;
        }
        scoreText.text = "Score: " + score;
        input.gameObject.SetActive(false);
        inputBG.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(false);
    }

    public void UpdateHealth(Health health) // changes the fill level and text of the player's health bar to match their current health
    {
        healthBar.fillAmount = health.CurrentHealth / health.maxHealth;
        if (health.CurrentHealth > 0)
        {
            healthText.text = health.CurrentHealth + "/" + health.maxHealth;
        }
        else
        {
            healthText.text = "Dead";
            healthText.color = Color.red;
        }
    }

    public void UpdateScore(int score) // updates the score text
    {
        scoreText.text = "Score: " + score;
    }

    public void UpdateTimer(float time) // updates the timer text and rounds the timer to 1 decimal place ("F1")
    {
        timerText.text = "CURRENT TIME: \n" + time.ToString("F1") + "s";
     }
    
    public void ResumeGame() // turns the cursor visibility off and locks it to the center of the screen, unpauses the agme, turns off the menu and resets the timeScale to 1
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        paused = false;
        pauseMenu.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
    public void PauseMenu() //inverts the pause menu, cursor visibility/lock states and the time scale depending on whether the game is paused or not {used for pressing escape to enter/exit the pause menu}
    {
        if (paused == true)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
            pauseMenu.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        if (paused == false)
        {
            pauseMenu.gameObject.SetActive(false);
            Time.timeScale = 1;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
