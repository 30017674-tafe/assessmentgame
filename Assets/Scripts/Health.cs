﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public float maxHealth = 100;

    public float CurrentHealth { get; private set; } = 0; // can be returned publicly in other fucntions, but set privately

    // Start is called before the first frame update
    void Start() // resets the current health to max, updates the healthbar and healthbar text to match
    {
        CurrentHealth = maxHealth;
        UIManager.instance.UpdateHealth(this);
        UIManager.instance.healthText.text = CurrentHealth + " / " + maxHealth;
    }

   public void TakeDamage(float damage) // applies the damage taken / healing received to the player
    {
        CurrentHealth -= damage;
        if (CurrentHealth > maxHealth)
        {
            CurrentHealth = maxHealth;
        }
        if(CurrentHealth <= 0) // if the player dies, turns the cursor back on and lets the player move it around, and loads the death scene
        {
            CurrentHealth = 0;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
            SceneManager.LoadScene(3);
        }
        UIManager.instance.UpdateHealth(this);
    }
}
