﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LogReader : MonoBehaviour
{
    public TextFileManager fileManager;
    // Start is called before the first frame update
    void Start() // creates a file with the given name and reads the contents of it
    {
        fileManager.CreateFile(fileManager.logName);
        fileManager.ReadFileContents(fileManager.logName);
    }
}

[System.Serializable]
public class TextFileManager
{
    public string logName;
    string[] logContents;

    /// <summary>
    ///  if  the file doesn't exist with the given file name in the diretory, it creats a creates a resources directory and creates a text file, with the first text being the file name and a new line
    /// </summary>
    /// <param name="fileName"></param>
    public void CreateFile(string fileName)
    {
        string filePath = Application.dataPath + "/Resources/" + fileName + ".txt";
        if (File.Exists(filePath) == false)
        {
            Directory.CreateDirectory(Application.dataPath + "/Resources/");
            File.WriteAllText(filePath, fileName + '\n');
        }
    }
    /// <summary>
    ///  creates an empty string to store the text contents, and sets the text contents to all lines of the filepath
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public string[] ReadFileContents(string fileName)
    {
        string filePath = Application.dataPath + "/Resources/" + fileName + ".txt";
        string[] textContents = new string[0];
        if (File.Exists(filePath) == true)
        {
            textContents = File.ReadAllLines(filePath);
        }
        logContents = textContents;
        return logContents;
    }

    public void AddKeyValuePair(string fileName, string key, string value) // adds the given key and the value (e.g the initals and the time) to the given file. appends the date and the content (key + value) to the end of the file
    {
        string filePath = Application.dataPath + "/Resources/" + fileName + ".txt";
        string content = key + "," + value;
        string dateStamp = System.DateTime.Today.ToString();
        if (File.Exists(filePath) == true)
        {
            bool contentsFound = false;
            for (int i = 0; i < logContents.Length; i++)
            {
                if(logContents[i].Contains(key + ",") == true)
                {
                    logContents[i] = dateStamp + " - " + content;
                    contentsFound = true;
                    break;
                }
            }
            if (contentsFound == true)
            {
                File.WriteAllLines(filePath, logContents);
            }
            else
            {
                File.AppendAllText(filePath, dateStamp + " - " + content + '\n');
            }
        }
    }

    public string LocateValueByKey(string key) // used to locate a score by the given key
    {
        string value = "";
        ReadFileContents(logName);
        foreach (string s in logContents)
        {
            if (s.Contains(key + ",") == true)
            {
                string[] splitString = s.Split(',');
                value = splitString[splitString.Length - 1];
                break;
            }
        }
        return value;
    }

    public string[] GetPairByKey(string key) // returns the key and value with a given key
    {
        string[] r = new string[0];
        string value = "";
        ReadFileContents(logName);
        foreach (string s in logContents)
        {
            if (s.Contains(key + ",") == true)
            {
                string[] splitString = s.Split(',');
                value = splitString[splitString.Length - 1];
                r = new string[] { key, value };
                break;
            }
        }
        return r;
    }

    public void ClearFile(string fileName) // clears the file contents
    {
        string filePath = Application.dataPath + "/Resources/" + fileName + ".txt";
        File.WriteAllText(filePath, fileName + '\n');
    }
}