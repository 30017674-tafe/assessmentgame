﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public float reach = 4.0f;

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit crosshairUpdate, reach) == true) // executes if raycast hits something
        {
            InteractableObject target = crosshairUpdate.transform.GetComponent<InteractableObject>();
            KeyTrigger key = crosshairUpdate.transform.GetComponent<KeyTrigger>();
            if (target == null) 
            {
                target = crosshairUpdate.transform.GetComponentInParent<InteractableObject>(); // checks the parent object of the target to see if it is interactable
            }
            if (target != null || key != null )
            {
                UIManager.instance.crosshair.sprite = UIManager.instance.interactableCrosshair; // updates the crosshair sprite if the object is interactable
            }
        }
        else
        {
            UIManager.instance.crosshair.sprite = UIManager.instance.baseCrosshair; // sets the crosshair to the default sprite if the raycast doesn't hit anything
        }
        if (Input.GetButtonDown("Use") == true)
        {
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit hit, reach) == true) // executes if raycast hits something
            {
                Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * reach, Color.white, 2.0f);
                InteractableObject obj = hit.transform.GetComponent<InteractableObject>();
                if (obj == null)
                {
                    obj = hit.transform.GetComponentInParent<InteractableObject>();
                }
                if (obj != null)
                {
                    if (obj is DoorInteraction door) // if the obj has a DoorInteraction script
                    {
                        if (door.locked == true)
                        {
                            if (GetComponent<KeyInv>().CheckKeys(door.keyColour) == true) // checks the keys in the key inventory to match the door's colour, unlocks if the key is in the inventory
                            {
                                door.UnlockDoor(true);
                            }
                            else
                            {
                                door.Activate(); // tries to open the door if the key isn't in the inventory
                            }
                        }
                        else
                        {
                            obj.Activate();
                        }
                    }
                }
            }
        }
    }
}
